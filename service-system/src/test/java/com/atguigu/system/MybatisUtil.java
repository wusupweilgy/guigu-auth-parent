package com.atguigu.system;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = ServiceAuthApplication.class)
public class MybatisUtil {

   @Test
    public void generate(){
       // 1、创建代码生成器
       AutoGenerator mpg = new AutoGenerator();

       // 2、全局配置
       // 全局配置
       GlobalConfig gc = new GlobalConfig();
       String projectPath = System.getProperty("user.dir");
       //gc.setOutputDir(projectPath + "/src/main/java");
       //这里是输出生成代码的目录，这里要填电脑的绝对路径（必改）+拼接出来的路径
       gc.setOutputDir("F:\\项目\\guigu-auth-parent\\service-system"+"/src/main/java");

       gc.setServiceName("%sService");	//去掉Service接口的首字母I
       gc.setAuthor("lgy");
       gc.setOpen(false);
       mpg.setGlobalConfig(gc);

       // 3、数据源配置
       DataSourceConfig dsc = new DataSourceConfig();
       dsc.setUrl("jdbc:mysql://localhost:3306/guigu-auth");
       dsc.setDriverName("com.mysql.cj.jdbc.Driver");
       dsc.setUsername("root");
       dsc.setPassword("mysql");
       dsc.setDbType(DbType.MYSQL);
       mpg.setDataSource(dsc);

       // 4、包配置
       PackageConfig pc = new PackageConfig();
       pc.setModuleName("system"); //模块名
       pc.setParent("com.atguigu");//包名，最后组合成 com.service_vod

       pc.setController("controller");
       pc.setService("service");
       pc.setMapper("mapper");
       mpg.setPackageInfo(pc);

       // 5、策略配置
       StrategyConfig strategy = new StrategyConfig();

       //具体生成哪些表要具体指定
       //strategy.setInclude("想要生成的表名");
       strategy.setInclude("sys_role");

       strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略

       strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
       strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

       strategy.setRestControllerStyle(true); //restful api风格控制器
       strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

       mpg.setStrategy(strategy);

       // 6、执行
       mpg.execute();
   }
}
