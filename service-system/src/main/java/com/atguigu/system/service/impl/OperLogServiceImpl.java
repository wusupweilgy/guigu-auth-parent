package com.atguigu.system.service.impl;

import com.atguigu.model.system.SysOperLog;
import com.atguigu.system.mapper.OperLogMapper;
import com.atguigu.system.service.OperLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OperLogServiceImpl implements OperLogService {

    @Resource
    private OperLogMapper sysOperLogMapper;
    @Override
    public void saveSysLog(SysOperLog operLog) {
        sysOperLogMapper.insert(operLog);
    }
}
