package com.atguigu.system.service;

import com.atguigu.model.system.SysOperLog;

public interface OperLogService {
    void saveSysLog(SysOperLog operLog);
}
